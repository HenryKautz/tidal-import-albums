
import tidalapi
import argparse  

import os
import re

import json
from math import ceil
from urllib.error import HTTPError 


DISCOGS_TOKEN = str(os.environ.get('DISCOGS_TOKEN'))

# Record = (title pattern, title replacement, artist pattern, artist replacement, year recorded, genre)
# Only title pattern is required 

REWRITE_DB = [ 
        ('William Bolcom: Piano Rags','','','Spencer Myer'),
        ('1923/24','1923/24','Jelly Roll Morton'),
        ('furry lewis 1927', 'In His Prime 1927-1928'),
        ('mill log blues', 'Jaybird Coleman & The Birmingham Jug Band'),
        ('black british swing', 'black british swing'),
        ('lil johnson', 'lil johnson vol. 1'),
        ('complete erskine hawkins','Anthology: the deluxe collection'),
        ('autumn breeze','milt jackson','milt jackson', 'milt jackson quartet'),
        ('in paris', 'american swinging in paris', 'don byas'),
        ('r&r','', 'ruby braff','ralph sutton'), 
        ]

def rewriter(artist:str, title:str)->tuple[str,str,int,str]:
    for (record) in REWRITE_DB:
        title_pattern = record[0]
        title_replacement = record[1] if len(record)>1 and record[1] else title
        artist_pattern = record[2] if len(record)>2 else ''
        artist_replacement = record[3] if len(record)>3 and record[3] else artist
        year = record[4] if len(record)>4 else 0
        genre = record[5] if len(record)>5 else ''

        if re.search(title_pattern, title) and re.search(artist_pattern, artist):
            return (artist_replacement, title_replacement, year, genre)
    
    return (artist, title, 0, '')


def anyin(a:str,b:str):
    aa = a.casefold()
    bb = b.casefold()
    return aa in bb or bb in aa

def albums_to_playlist(
        in_datafile:str='',

        save:bool=False
        ):
    
    # Create Tidal client
    session = tidalapi.Session()
    # Will run until you visit the printed url and link your account
    session.login_oauth_simple()
    me = session.user
    tidal_id = me.id #type:ignore

    #
    # Set up album database
    #

    albums_db = []
    with open(in_datafile,'r') as f:
        albums_db = json.loads(f.read())
    print(f"Read album database {in_datafile}")

    favorites = tidalapi.user.Favorites(session,tidal_id)
    done = False
    offset = 0
    tidal_albums = []
    while not done: 
        items = favorites.albums(limit=50, offset=offset)
        if len(items)<50:
            done = True
        tidal_albums += items
        offset += len(items)
        print(f"Downloaded {offset} albums...")

    album_is_in_tidal = set()
    for a in tidal_albums:
        # artist = re.sub(r"'", '', a.artists[0].name.casefold())
        # title = re.sub(r"'", '', a.name.casefold())        
        album_is_in_tidal.add((a.artists[0].name.casefold(),a.name.casefold()))
    print(f"Found {len(tidal_albums)} saved albums in Tidal")

    for a in albums_db:
        artist = a['album_artist']
        title = a['album_title']
        (artist,title,_,_) = rewriter(artist,title)
        artist = artist.casefold()
        title = title.casefold()
        # title = re.sub(r"'", '', title)
        # artist = re.sub(r"'", '', artist)

        if not (artist,title) in album_is_in_tidal:
            # Search for album in Tidal
            album_id = None
            hits = session.search(title, models=[tidalapi.album.Album])
            if not hits['top_hit']:
                title = re.sub(r'\(.*\)', '', title)
                title = re.sub(r'\[].*\]', '', title)
                hits = session.search(title, models=[tidalapi.album.Album])
            if hits['top_hit']:
                tophit = hits['top_hit']
                if anyin(tophit.artists[0].name, artist):
                    album_id = tophit.id
                elif len(tophit.artists)>1 and anyin(tophit.artists[1].name, artist):
                    album_id = tophit.id
                else:
                    for a in hits['albums']:
                        if anyin(a.artists[0].name,artist):
                            album_id = a.id
                            break
                        if len(a.artists)>1 and anyin(a.artists[1].name,artist):
                            album_id = a.id
                            break
            # If found, favorite it, otherwise print warning
            if album_id:
                if save:
                    favorites.add_album(album_id)
                # print('+',end='',flush=True)
                print(f"Adding {artist}, {title} to favorites")
            else:
                print(f"WARNING: {artist}, {title} not found in Tidal")
    print()

def main():
    parser = argparse.ArgumentParser(description="Upload saved albums from file to Tidal")
    parser.add_argument('--save', '-s', action='store_true', help='Save playlist to Spotify, otherwise is a dry run')
    parser.add_argument('--read', '-r', type=str, default='albumsdb.json', metavar='FILE', help='import album database from a file')
    args = parser.parse_args()
    in_datafile = args.read
    
    albums_to_playlist( 
        in_datafile=in_datafile,
        save=args.save
        )

if __name__ == '__main__':
    main()

