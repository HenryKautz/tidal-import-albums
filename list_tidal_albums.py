import argparse
import tidalapi

def list_albums(file=''):
    # Create Tidal client
    session = tidalapi.Session()
    # Will run until you visit the printed url and link your account
    session.login_oauth_simple()
    me = session.user
    tidal_id = me.id #type:ignore

    #
    # Set up album database
    #

    favorites = tidalapi.user.Favorites(session,tidal_id)
    done = False
    offset = 0
    tidal_albums = []
    while not done: 
        items = favorites.albums(limit=50, offset=offset)
        if len(items)<50:
            done = True
        tidal_albums += items
        offset += len(items)
        print(f"Downloaded {offset} albums...")

    print(f"Number of albums = {len(tidal_albums)}")
    tidal_albums.sort(key=lambda x: x.artists[0].name)

    i = 1
    for a in tidal_albums:
        artist = a.artists[0].name
        title = a.name
        print(f"{i:3n}. {artist}, {title}")
        i += 1

    if file:
        with open(file,'w') as f:
            i = 1
            for a in tidal_albums:
                artist = a.artists[0].name
                title = a.name
                print(f"{i:3n}. {artist}, {title}", file=f)
                i += 1

def main():
    parser = argparse.ArgumentParser(description="List favorite Tidal albums")
    parser.add_argument('--file', '-f', type=str, default='', help='Print list of albums to file')
    args = parser.parse_args()
    list_albums(args.file)

if __name__ == '__main__':
    main()

